/* author: Carolina de Senne Garcia
 * RA 145656
 * desennecarol@gmail.com
 *
 * compile this code with g++
 * g++ batman.cpp -lpthread -o batman
 * /usr/local/bin/g++-9 on macOs (install with brew install gcc)
 *
 * run this program
 * ./batman [K=<nice integer>] [-m]
 */

#include <iostream>
#include <pthread.h>
#include <thread>
#include <signal.h>
#include <math.h>
#include <string>
#include <list>
#include <regex>
#include <queue>
#include <mutex>
#include <condition_variable>
using namespace std;

// BlockingQueue implements a blocking queue that is thread safe (pop and front wait until there is an element in the queue).
template<typename T>
class BlockingQueue {
private:
    mutex    			m;
    condition_variable	not_empty;
    queue<T>    		q;
public:
	int size() {
        unique_lock<mutex> lock(m);
        return q.size();
    }

    bool empty() {
    	unique_lock<mutex> lock(m);
        return q.empty();
    }
    void push(T const& value) {
    	{
    		unique_lock<mutex> lock(m);
       		q.push(value);
    	}
        not_empty.notify_all();
    }
    T front() {
    	unique_lock<mutex> lock(m);
    	while (q.size()==0) {
            not_empty.wait(lock);
        }
    	return q.front();
    }
    void pop() {
    	unique_lock<mutex> lock(m);
    	while (q.size()==0) {
            not_empty.wait(lock);
        }
        q.pop();
    }
};

// North, East, South, West define the queues from each direction on the crossing.
BlockingQueue<int> North;  // North ID=0
BlockingQueue<int> East;   // East  ID=1
BlockingQueue<int> South;  // South ID=2
BlockingQueue<int> West;   // West  ID=3

// K is the limit of the size of a queue before it goes gentle and lets others pass.
int K = 2;
// M indicates whether there are multiple crossing at a time (North with South, East with West).
bool M=false;
// turn indicates the id of which direction can cross now.
int turn;
// cross_mutex protects the access to turn.
mutex cross_mutex;
// wantsToCross is used by each queue to indicate its interest to cross.
bool wantsToCross[4];
// ask_cross notifies manager that someone is willing to cross.
condition_variable ask_cross;
// allow_cross notifies the queues that one crossing is allowed by the manager.
condition_variable allow_cross;
// yield indicates whether the queue with id i wants to yield.
bool yield[4];

// Bitmask implements a thread-safe bitmask for setting/checking the presence/absence of BATs on the queues.
class Bitmask {
	private:
		/* bm represents the presence/absence of BATs on the queues.
		 * this information is stored in the 4 right-most bits.
		 * 0001 = 1 means only North has BATs.
		 * 0010 = 2 means only East has BATs.
		 * 0100 = 4 means only South has BATs.
		 * 1000 = 8 means only West has BATs.
		 * 1001 = 9 means North and West have BATs.
		 * and so on...
		 */
		int bm;
		mutex m;
	public:
		Bitmask() {
			bm=0;
		}
		// reads bitmask (used by queue consumers). 
		// we block the reading because it is easier to implement it this way.
		int read() {
			m.lock();
			int b = bm;
			m.unlock();
			return b;
		}
		// writes bitmask according to emptyness of queues (used by manager).
		void write() {
			int acc = 0;
			for(int i = 0; i < 4; i++) {
				if(wantsToCross[i]) {
					acc = acc | (1<<i);
				}
			}
			m.lock();
			bm = acc;
			m.unlock();
		}

};

// bitmask checks the presence or absence of BATs in the queues.
Bitmask bitmask;

// bat_producer reads input from stdin and puts bats on their respective the queues.
void *bat_producer(void*) {
	int bat_count = 1;
	for (string line; getline(cin, line);) {
		cross_mutex.lock();
        for(int i=0; i<line.size(); i++) {
        	switch (line[i]) {
        		case 'n': North.push(bat_count); break;
        		case 'e': East.push(bat_count); break;
        		case 's': South.push(bat_count); break;
        		case 'w': West.push(bat_count); break;
        	}
        	bat_count++;
        }
        cross_mutex.unlock();
    }
    pthread_exit(NULL);
}

void *queue_consumer(void *queueid) {
	BlockingQueue<int> *q;
	char direction;
	long id = (long)queueid;
	switch (id) {
		case 0: q=&North; direction='N'; break;
		case 1: q=&East; direction='E'; break;
		case 2: q=&South; direction='S'; break;
		case 3: q=&West; direction='W'; break;
	}
	while(true) {
		// front waits until there is some BAT in the head of the queue.
		int bat = q->front();
		unique_lock<mutex> lock(cross_mutex);
		cout << "BAT " << bat << " " << direction << " chegou no cruzamento" << endl;
		// notifies manager that it wants to cross.
		wantsToCross[id] = true;
		ask_cross.notify_one();
		// wait for permission to cross.
		do {
			allow_cross.wait(lock);
		} while(!(turn==id || (M && ((turn+2)%4==id)))); // if M is turned on, then the other direction is also a valid turn.
		// checks if wants to yield or not. Notifies manager of decision.
		// yield: give the place to someone else in the crossing.
		// conditions: 1. shouldnt have yielded on the last turn.
		//			   2. shouldnt be the only non empty queue.
		//			   3. other sense in same direction shouldnt be the only one waiting (if M).
		//			   4. size should be greater than K.
		bool b = (!yield[id]) && (turn==id) && (bitmask.read()!=0) && (q->size()>K) && bitmask.read()!=(1<<id);
		b = b && ((M && (bitmask.read()&((1<<((id+1)%4)) + (1<<((id+3)%4))))!=0) || !M);
		if (b) {
			yield[id] = true;
			cout << "BAT " << bat << " " << direction << " cedeu passagem" << endl;
		} else {
			yield[id] = false;
		}
		ask_cross.notify_one();
		// wait for permission to cross again.
		do {
			allow_cross.wait(lock);
		} while(!(turn==id || (M && ((turn+2)%4==id))));
		// bat crosses the crossing.
		cout << "BAT " << bat << " " << direction << " saiu do cruzamento" << endl;
		q->pop();
		wantsToCross[id]=false;
		ask_cross.notify_one();
	}
	pthread_exit(NULL);
} 

void print_impasse(int bm, int turn) {
	// must print an IMPASSE if: 1. there are other people in the crossing.
	//							 2. they are all of lower priotity than turn.
	bool b = ((bm & ((1<<((turn+1)%4))+(1<<((turn+3)%4))))!=0 && M) || !M;
	if(bm!=0 && bm!=1 && bm!=2 && bm!=4 && bm!=8 && bm>=(1<<(turn+1)) && b) {
		char direction;
		switch(turn) {
			case 0: direction='N'; break;
			case 1: direction='E'; break;
			case 2: direction='S'; break;
			case 3: direction='W'; break;
		}
		list<char> impasses;
		if ((bm & 1) && turn==0) {impasses.push_back('N');}
		if ((bm & 2) && turn<=1) {impasses.push_back('E');}
		if ((bm & 4) && turn<=2) {impasses.push_back('S');}
		if ((bm & 8) && turn<=3) {impasses.push_back('W');}
		cout << "Impasse: ";
		while(impasses.size()>1) {
			cout << impasses.front() << ",";
			impasses.pop_front();
		}
		cout << impasses.front();
		cout << " sinalizando " << direction << " para ir" << endl;
	}
}

void *manager(void*) {
	while(true) {
		unique_lock<mutex> lock(cross_mutex);
		// waits until someone is in the queue to cross.
		while(!(wantsToCross[0] || wantsToCross[1] || wantsToCross[2] || wantsToCross[3])) {
			ask_cross.wait(lock);
		}
		bitmask.write();
		// choose next queue to cross.
		turn = 0;
		while((bitmask.read()&(1<<turn))==0) {
			turn = (turn+1)%4;
		}
		bool nobody_crossed = true;
		do {
			// wake up all queue threads.
			allow_cross.notify_all();
			// wait for signal from thread whose id=turn.
			ask_cross.wait(lock);
			// if it is yielding, choose next in the list.
			if (yield[turn]) {
				do {
					turn = (turn+1)%4;
				} while((bitmask.read()&(1<<turn))==0);
			} else {
				nobody_crossed=false;
				// print impasse
				print_impasse(bitmask.read(),turn);
				allow_cross.notify_all();
				ask_cross.wait(lock); // wait for it to pass
			}
		} while(nobody_crossed);
	}
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	// set K if passed by command line argument. Otherwise, K=2 (default value).
	// set M if passed by command line argument. Otherwise, M=false (default value).
	if (argc > 1) {
  		string s = argv[1];
  		if (s[0]=='K' && s[1]=='=') {
  			K = stoi(&s[2]);
  		} else if(s[0]=='-' && s[1]=='m') {
  			M=true;
  		} else {
  			cout << "Usage: ./batman [K=<some integer>] [-m]" << endl;
        	exit(-1);
  		}
  		if(argc > 2) {
  			string s1 = argv[2];
  			if(s1[0]=='-' && s1[1]=='m') {
  				M=true;
  			} else {
  				cout << "Usage: ./batman [K=<some integer>] [-m]" << endl;
        		exit(-1);
  			}
  		}
	}

	// manager manages queues crossings.
	pthread_t man;
	int m = pthread_create(&man, NULL, manager, NULL);
	if (m) {
         cout << "Error:unable to create manager thread," << m << endl;
         exit(-1);
    }

	// the queue<Direction> thread is the consumer for the respective queue.
	pthread_t queueNorth;
	pthread_t queueEast;
	pthread_t queueSouth;
	pthread_t queueWest;
	int qn = pthread_create(&queueNorth, NULL, queue_consumer, (void *)0);
	int qe = pthread_create(&queueEast, NULL, queue_consumer, (void *)1);
	int qs = pthread_create(&queueSouth, NULL, queue_consumer, (void *)2);
	int qw = pthread_create(&queueWest, NULL, queue_consumer, (void *)3);
	if (qn || qe || qs || qw) {
		cout << "Error:unable to create queue threads," << qn << qe << qs << qw << endl;
        exit(-1);
	}

	// reader reads lines from input and feeds the queues.
	pthread_t reader;
	int r = pthread_create(&reader, NULL, bat_producer, NULL);
	if (r) {
         cout << "Error:unable to create reading thread," << r << endl;
         exit(-1);
    }
    // wait for reader to finish.
    pthread_join(reader, NULL);

    // wait until queues are empty.
    while(!(North.empty() && East.empty() && South.empty() && West.empty())) {
    	this_thread::sleep_for(chrono::milliseconds(100));
    }
    // kill other threads (queues and manager).
    pthread_kill(man,SIGKILL);
    pthread_kill(queueNorth,SIGKILL);
    pthread_kill(queueEast,SIGKILL);
    pthread_kill(queueSouth,SIGKILL);
    pthread_kill(queueWest,SIGKILL);

    pthread_exit(NULL);
}
